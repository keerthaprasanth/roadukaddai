//
//  OtpVerificationViewController.swift
//  Roadukaddai
//
//  Created by Keertha Prasanth on 26/07/18.
//  Copyright © 2018 Keertha Prasanth. All rights reserved.
//

import UIKit
import FirebaseAuth

struct API {
    
    static let APIKey = "736d41c23d6ca360745d9ab7cf356cf3"
    static let BaseURL = URL(string: "https://api.darksky.net/forecast/")!
   // https://api.darksky.net/forecast/736d41c23d6ca360745d9ab7cf356cf3/37.8267,-122.4233
    static var AuthenticatedBaseURL: URL {
        return BaseURL.appendingPathComponent(APIKey)
    }
    
}

class OtpVerificationViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var temparatureValue: UILabel!
    @IBOutlet weak var verifyandProcessButton: UIButton!
    @IBOutlet weak var otpTextField1: UITextField!
    @IBOutlet weak var otpTextField2: UITextField!
    @IBOutlet weak var otpTextField3: UITextField!
    @IBOutlet weak var otpTextField4: UITextField!
    @IBOutlet weak var otpTextField5: UITextField!
    @IBOutlet weak var otpTextField6: UITextField!

    private let dataManager = DataManager(baseURL: API.AuthenticatedBaseURL)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setupView()
        self.otpTextField1.tintColor = UIColor.clear

        self.otpTextField2.tintColor = UIColor.clear

        self.otpTextField3.tintColor = UIColor.clear

        self.otpTextField4.tintColor = UIColor.clear

        self.otpTextField5.tintColor = UIColor.clear

        self.otpTextField6.tintColor = UIColor.clear
        
//        otpTextField1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
//        otpTextField2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
//        otpTextField3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
//        otpTextField4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
//        otpTextField5.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
//        otpTextField6.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        
// Fetch Weather Data
        dataManager.weatherDataForLocation(latitude: 10.9346, longitude: 76.9787) { (response, error) in
            print(response!)
            if let values = response?.value(forKey: "currently")
            {
                if let temp = (values as AnyObject).value(forKey: "temperature")
                {
                    print(temp)
                    let stringRepresentation = (temp as AnyObject).description // "[1, 2, 3]"
//                    let sting = stringRepresentation?.prefix(2)
                    var celsi = Int()
                    celsi = Int(Double(stringRepresentation!)!)
                    DispatchQueue.main.async {
                        let celsius = (celsi - 32) * 5/9
                        let string_to_int = Double(celsius)
                        let sting = floor(string_to_int)
                        var stringValue = String()
                        stringValue = String(sting)
                        self.temparatureValue.text = "\((stringValue.prefix(2)))°"
                        print(self.temparatureValue.text!)
                    }
                }
            }
   
        }
        
        
        
        verifyandProcessButton.isUserInteractionEnabled = false
        verifyandProcessButton.backgroundColor = UIColor.gray
        
        self.addDoneButtonOnKeyboard()

        
        otpTextField1.becomeFirstResponder()
        self.navigationController?.navigationBar.isHidden = false
        let bottomLine = UILabel()
        bottomLine.frame = CGRect(x: otpTextField1.frame.origin.x, y:otpTextField1.frame.origin.y +  otpTextField1.frame.height - 1, width: otpTextField1.frame.width,height: 2.0)
        bottomLine.backgroundColor = UIColor.orange
        
        let bottomLine1 = UILabel()
        bottomLine1.frame = CGRect(x: otpTextField2.frame.origin.x, y: otpTextField2.frame.origin.y +  otpTextField2.frame.height - 1, width: otpTextField2.frame.width,height: 2.0)
        bottomLine1.backgroundColor = UIColor.orange
        
        let bottomLine2 = UILabel()
        bottomLine2.frame = CGRect(x: otpTextField3.frame.origin.x, y:otpTextField3.frame.origin.y +   otpTextField3.frame.height - 1, width: otpTextField3.frame.width,height: 2.0)
        bottomLine2.backgroundColor = UIColor.orange
        
        let bottomLine3 = UILabel()
        bottomLine3.frame = CGRect(x: otpTextField4.frame.origin.x, y:otpTextField4.frame.origin.y +   otpTextField4.frame.height - 1, width: otpTextField4.frame.width,height: 2.0)
        bottomLine3.backgroundColor = UIColor.orange
        
        let bottomLine4 = UILabel()
        bottomLine4.frame = CGRect(x: otpTextField5.frame.origin.x, y:otpTextField5.frame.origin.y +   otpTextField5.frame.height - 1, width: otpTextField5.frame.width,height: 2.0)
        bottomLine4.backgroundColor = UIColor.orange
        
        let bottomLine5 = UILabel()
        bottomLine5.frame = CGRect(x: otpTextField6.frame.origin.x, y: otpTextField6.frame.origin.y +  otpTextField6.frame.height - 1, width: otpTextField6.frame.width,height: 2.0)
        bottomLine5.backgroundColor = UIColor.orange
        
         otpTextField1.borderStyle = UITextBorderStyle.none
        otpTextField2.borderStyle = UITextBorderStyle.none
        otpTextField3.borderStyle = UITextBorderStyle.none
        otpTextField4.borderStyle = UITextBorderStyle.none
        otpTextField5.borderStyle = UITextBorderStyle.none
        otpTextField6.borderStyle = UITextBorderStyle.none

        view.addSubview(bottomLine)
        view.addSubview(bottomLine1)
        view.addSubview(bottomLine2)
        view.addSubview(bottomLine3)
        view.addSubview(bottomLine4)
        view.addSubview(bottomLine5)

        otpTextField1.tag = 1
        otpTextField2.tag = 2
        otpTextField3.tag = 3
        otpTextField4.tag = 4
        otpTextField5.tag = 5
        otpTextField6.tag = 6

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case otpTextField1:
                otpTextField2.becomeFirstResponder()
            case otpTextField2:
                otpTextField3.becomeFirstResponder()
            case otpTextField3:
                otpTextField4.becomeFirstResponder()
            case otpTextField4:
                otpTextField5.becomeFirstResponder()
            case otpTextField5:
                otpTextField6.becomeFirstResponder()
            case otpTextField6:
                otpTextField6.resignFirstResponder()
            default:
                break
            }
        }
        if  text?.count == 0 {
            switch textField{
            case otpTextField1:
                otpTextField1.becomeFirstResponder()
            case otpTextField2:
                otpTextField1.becomeFirstResponder()
            case otpTextField3:
                otpTextField2.becomeFirstResponder()
            case otpTextField4:
                otpTextField3.becomeFirstResponder()
            case otpTextField5:
                otpTextField4.becomeFirstResponder()
            case otpTextField6:
                otpTextField5.becomeFirstResponder()
            default:
                break
            }
        }
        else{
            
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField.text!.count < 1  && string.count > 0{
            let nextTag = textField.tag + 1

            // get next responder
            var nextResponder = textField.superview?.viewWithTag(nextTag)

            if (nextResponder == nil){

                nextResponder = textField.superview?.viewWithTag(1)
            }
            if (nextTag <= 6)
            {
                textField.text = string
                //textField.isUserInteractionEnabled = false
            nextResponder?.becomeFirstResponder()
            }
            else if (nextTag == 7)
            {
                textField.text = string
                verifyandProcessButton.isUserInteractionEnabled = true
                verifyandProcessButton.backgroundColor = UIColor.orange
                nextResponder?.resignFirstResponder()
                self.view.endEditing(true)
                phoneNumberVerification()
            }
            return false
        }
        else if textField.text!.count >= 1  && string.count == 0{
            // on deleting value from Textfield
            let previousTag = textField.tag - 1

            // get next responder
            var previousResponder = textField.superview?.viewWithTag(previousTag)

            if (previousResponder == nil){
                previousResponder = textField.superview?.viewWithTag(1)
            }
            textField.text = ""
            //previousResponder?.isUserInteractionEnabled = true
            verifyandProcessButton.isUserInteractionEnabled = false
            verifyandProcessButton.backgroundColor = UIColor.gray
            previousResponder?.becomeFirstResponder()
            return false
        }
//        else
//        {
//            let nextTags = textField.tag + 1
//            if (nextTags == 7)
//            {
//                return false
//            }
//            else
//            {
//                // get next responder
//                var nextResponder = textField.superview?.viewWithTag(nextTags)
//                print("my text", textField.text!)
//
//                if (nextResponder == nil){
//
//                    nextResponder = textField.superview?.viewWithTag(1)
//                }
//                if (nextTags <= 6)
//                {
//                    textField.text = string
//                    nextResponder?.becomeFirstResponder()
//                }
//            }
            return false
//
//        }
    }

    @IBAction func varifyAndContinue(_ sender: Any)
    {
        phoneNumberVerification()
    }

    func phoneNumberVerification()
    {
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        let verificationCode = otpTextField1.text! + otpTextField2.text!  + otpTextField3.text!  + otpTextField4.text! + otpTextField5.text!  + otpTextField6.text!
        print(verificationID!)
        print(verificationCode)
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID!,
            verificationCode: verificationCode)
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                // ...
                print(error.localizedDescription)
                return
            }
            else
            {
                print(authResult!)
//                let udid = authResult?.user.uid
//                UserDefaults.standard.set(udid, forKey: "Usersudid")
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardViewController") as? DashBoardViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            // User is signed in
            // ...
        }
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.otpTextField1.inputAccessoryView = doneToolbar
        self.otpTextField2.inputAccessoryView = doneToolbar
        self.otpTextField3.inputAccessoryView = doneToolbar
        self.otpTextField4.inputAccessoryView = doneToolbar
        self.otpTextField5.inputAccessoryView = doneToolbar
        self.otpTextField6.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction()
    {
        self.otpTextField1.resignFirstResponder()
        self.otpTextField2.resignFirstResponder()
        self.otpTextField3.resignFirstResponder()
        self.otpTextField4.resignFirstResponder()
        self.otpTextField5.resignFirstResponder()
        self.otpTextField6.resignFirstResponder()
        
    }
    
}
