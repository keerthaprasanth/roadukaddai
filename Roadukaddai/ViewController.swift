//
//  ViewController.swift
//  Roadukaddai
//
//  Created by Keertha Prasanth on 24/07/18.
//  Copyright © 2018 Keertha Prasanth. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController,UIScrollViewDelegate,UITextViewDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var haveAnAccount: UILabel!
   
    @IBOutlet weak var setDeliveryLocationButton: UIButton!
    @IBOutlet weak var scrollTexts: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    var loginImages: [UIImage] = [     //Array of Banners
        UIImage(named: "1.jpg")!,
        UIImage(named: "2.jpg")!,
        UIImage(named: "3.jpg")!
        ]
    var blurEffect = UIBlurEffect(style: UIBlurEffectStyle.extraLight)
    var blurEffectView = UIVisualEffectView()
    var phoneTextField = UITextField()

    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //view.bringSubview(toFront: image)
        
        self.navigationController?.navigationBar.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        
        loadBlurEffectiew()
        // Do any additional setup after loading the view, typically from a nib.
        scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width * 3, height: self.scrollView.frame.size.height)
        self.pageControl.numberOfPages = loginImages.count
        self.pageControl.currentPage = 0
        scrollView.isPagingEnabled = true
        //Ajout des covers classiques
        for i in 0..<loginImages.count {
            var frame = CGRect()
            frame.origin.x = scrollView.frame.size.width * CGFloat(i)
            frame.origin.y = 0
            frame.size = scrollView.frame.size
            //Vue 1
            let subview1 = UIView(frame: frame)
            let imageView2 = UIImageView()
            let image2 = loginImages[i]
            imageView2.frame = subview1.frame
            imageView2.image = image2
            scrollView.addSubview(imageView2)
        }
        _ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(moveToNextImage), userInfo: nil, repeats: true)
        let scrollViewTap = UITapGestureRecognizer(target: self, action: #selector(removeSubViews))
        scrollViewTap.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(scrollViewTap)
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func removeSubViews()
    {
        let subViews = blurEffectView.subviews
        for subview in subViews
        {
            subview.isHidden = true
        }
        self.phoneTextField.resignFirstResponder()
    }
    
    @objc func moveToNextImage ()
    {
        let pageWidth:CGFloat = self.scrollView.frame.width
        let maxWidth:CGFloat = pageWidth * 3
        let contentOffset:CGFloat = self.scrollView.contentOffset.x

        var slideToX = contentOffset + pageWidth

        if  contentOffset + pageWidth == maxWidth {
            slideToX = 0
            pageControl.currentPage = 0
        }

        self.scrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.scrollView.frame.height), animated: true)
        if slideToX == 375
        {
        pageControl.currentPage = 1
        }
        else if slideToX == 750
        {
            pageControl.currentPage = 2
        }
    }

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let offsetLooping: Int = 1
        let page: Int = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + CGFloat(offsetLooping))
        pageControl.currentPage = page % loginImages.count
        //removeSubViews()
        //self.phoneTextField.resignFirstResponder()
    }
    
    @IBAction func setDeliveryLocationButtonAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardViewController") as? DashBoardViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if(touch?.view != blurEffectView.contentView)
        {
            let subViews = blurEffectView.subviews
            for subview in subViews
            {
                subview.isHidden = true
            }
            self.phoneTextField.resignFirstResponder()
        }
        
    }
 
    override func viewDidLayoutSubviews() {
        pageControl.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }
    }
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        UIView.animate(withDuration: 2.0, delay: 0.5, options: [.transitionFlipFromRight],
                       animations:{
                        let subViews = self.blurEffectView.subviews
                        for subview in subViews
                        {
                            subview.isHidden = false
                        }
        }, completion: nil)
        self.view.addSubview(self.blurEffectView)
        self.phoneTextField.becomeFirstResponder()
    }
   func loadBlurEffectiew()
    {
        self.blurEffectView.effect = self.blurEffect
        self.blurEffectView.frame = CGRect(x: 0, y: (self.view.frame.size.height / 2 - 120), width: self.view.frame.size.width, height: 240)
        self.blurEffectView.center = CGPoint(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 2)
        
        
        let label = UILabel()
        label.frame = CGRect(x: 20, y: 20, width:100 , height: 30)
        label.textAlignment = NSTextAlignment.left
        label.text = "LOGIN"
        label.font = UIFont(name:"Arial Rounded MT Bold", size: 22.0)
        label.font = UIFont.boldSystemFont(ofSize: 22.0)
        
        
        let subText = UILabel()
        subText.frame = CGRect(x: 20, y: label.frame.origin.y + label.frame.size.height + 2, width:self.view.frame.size.width , height: 20)
        subText.textAlignment = NSTextAlignment.left
        subText.text = "Enter your phone number to proceed"
        subText.font = UIFont(name:"Arial", size: 13.0)
        //subText.font = UIFont.boldSystemFont(ofSize: 13.0)
        
        let sublabel = UILabel()
        sublabel.frame = CGRect(x: 20, y: subText.frame.origin.y + subText.frame.size.height + 25, width:self.view.frame.size.width , height: 18)
        sublabel.textAlignment = NSTextAlignment.left
        sublabel.text = "PHONE NUMBER"
        sublabel.font = UIFont(name:"Arial", size: 13.0)
        sublabel.font = UIFont.boldSystemFont(ofSize: 13.0)
        
        phoneTextField.frame = CGRect(x: 20, y: sublabel.frame.origin.y + sublabel.frame.size.height + 5, width: self.view.frame.size.width - 40, height: 30)
        phoneTextField.textAlignment = .left
        phoneTextField.font = UIFont(name:"Arial Rounded MT Bold", size: 18.0)
        phoneTextField.text = "+91 "
        phoneTextField.keyboardType = .numberPad
        phoneTextField.returnKeyType = .done
        phoneTextField.delegate = self
//        phoneTextField.layer.borderWidth = 2
//        phoneTextField.layer.borderColor = UIColor.orange.cgColor
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: phoneTextField.frame.height - 1, width: phoneTextField.frame.width,height: 1.0)
        bottomLine.backgroundColor = UIColor.orange.cgColor
        phoneTextField.borderStyle = UITextBorderStyle.none
        phoneTextField.layer.addSublayer(bottomLine)
        
        let confirmButton = UIButton()
        confirmButton.frame = CGRect(x: 20.0, y: phoneTextField.frame.height + phoneTextField.frame.origin.y + 20, width: phoneTextField.frame.width,height: 40.0)
        confirmButton.backgroundColor = UIColor.orange
        confirmButton.addTarget(self, action: #selector(confirmButtonClicked), for: .touchUpInside)
        confirmButton.setTitle("CONFIRM", for: .normal)
        confirmButton.setTitleColor(UIColor.white, for: .normal)
        self.blurEffectView.contentView.addSubview(confirmButton)
        self.blurEffectView.contentView.addSubview(phoneTextField)
        self.blurEffectView.contentView.addSubview(sublabel)
        self.blurEffectView.contentView.addSubview(label)
        self.blurEffectView.contentView.addSubview(subText)
    }
    @objc func confirmButtonClicked() {
        
//        Auth.auth().languageCode = "ta";
////        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
//        PhoneAuthProvider.provider().verifyPhoneNumber(phoneTextField.text!, uiDelegate: nil) { (verificationID, error) in
//            if let error = error {
//                print(error.localizedDescription)
//                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
//                return
//            }
//            else
//            {
            //    UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                print("success")
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OtpVerificationViewController") as? OtpVerificationViewController
                self.navigationController?.pushViewController(vc!, animated: true)
//            }
////             Sign in using the verificationID and the code sent to the user
////             ...
//       }
        removeSubViews()
        self.phoneTextField.resignFirstResponder()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.blurEffectView.frame.origin.y == (self.view.frame.size.height / 2 - 120){
                let sizeofblurView = self.blurEffectView.frame.origin.y - keyboardSize.height
                self.blurEffectView.frame.origin.y -= sizeofblurView
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.blurEffectView.frame.origin.y != (self.view.frame.size.height / 2 - 120){
               // let sizeofblurView = self.blurEffectView.frame.origin.y + keyboardSize.height
               // self.blurEffectView.frame.origin.y += sizeofblurView
                removeSubViews()
                self.phoneTextField.resignFirstResponder()
            }
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        if (currentString.length == 4) && string == ""
        {
            phoneTextField.rightView = nil
            phoneTextField.rightViewMode = UITextFieldViewMode.never
            return false
        }
        else if (currentString.length >= 4)
        {
        let maxLength = 14
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if newString.length >= 14
        {
            phoneTextField.rightViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage(named: "1.jpg")
            imageView.tintColor = UIColor.red
            phoneTextField.rightView = imageView
        }
            else
        {
            phoneTextField.rightView = nil
            phoneTextField.rightViewMode = UITextFieldViewMode.never
            }
        
        return newString.length <= maxLength
        }
        phoneTextField.rightView = nil
        phoneTextField.rightViewMode = UITextFieldViewMode.never
        return false
    }
}

